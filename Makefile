#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: acrosnie <acrosnie@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/11/27 12:31:31 by acrosnie          #+#    #+#              #
#    Updated: 2014/01/31 19:38:20 by acrosnie         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME = out

CC = g++ -g -Werror -Wall -Werror -O3

DIR_GLUT = /System/Library/Frameworks/GLUT.framework/GLUT

DIR_OPENGL = /System/Library/Frameworks/OpenGL.framework/OpenGL

SRC = main.cpp

OBJ = $(SRC:.cpp=.o)

all: $(NAME)

$(NAME): $(OBJ)
	$(CC) $(DIR_GLUT) $(DIR_OPENGL) $(OBJ) -o $(NAME) $(LDFLAGS)

%.o: %.cpp fdf_42.h
	$(CC) -o $@ -c $<

clean:
	rm -rf $(OBJ)

fclean: clean
	rm -f $(NAME)

re: fclean all

.PHONY: clean all re fclean
